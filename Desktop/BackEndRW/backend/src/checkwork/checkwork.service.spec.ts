import { Test, TestingModule } from '@nestjs/testing';
import { CheckWorkService } from './checkwork.service';

describe('UsersService', () => {
  let service: CheckWorkService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [CheckWorkService],
    }).compile();

    service = module.get<CheckWorkService>(CheckWorkService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
