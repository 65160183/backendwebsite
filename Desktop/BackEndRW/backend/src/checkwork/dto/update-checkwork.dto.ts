import { PartialType } from '@nestjs/swagger';
import { CreateCheckworkDto } from './create-checkwork.dto';

export class UpdateCheckWorkDto extends PartialType(CreateCheckworkDto) {}
