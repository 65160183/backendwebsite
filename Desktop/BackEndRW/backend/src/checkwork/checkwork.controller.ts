import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
} from '@nestjs/common';
// eslint-disable-next-line @typescript-eslint/no-unused-vars
import { CheckWorkService } from './checkwork.service';
import { CreateCheckworkDto } from './dto/create-checkwork.dto';
import { UpdateCheckWorkDto } from './dto/update-checkwork.dto';

@Controller('CheckWork')
export class CheckWorkController {
  constructor(private readonly CheckWorkService: CheckWorkService) {}
  // Create
  @Post()
  create(@Body() createUserDto: CreateCheckworkDto) {
    return this.CheckWorkService.create(createUserDto);
  }

  // Read All
  @Get()
  findAll() {
    return this.CheckWorkService.findAll();
  }
  // Read One
  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.CheckWorkService.findOne(+id);
  }
  // Partial Update
  @Patch(':id')
  update(@Param('id') id: string, @Body() updateUserDto: UpdateCheckWorkDto) {
    return this.CheckWorkService.update(+id, updateUserDto);
  }
  // Delete
  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.CheckWorkService.remove(+id);
  }
}
