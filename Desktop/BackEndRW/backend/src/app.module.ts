import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DataSource } from 'typeorm';
import { User } from './users/entities/user.entity';
import { TemperatureModule } from './temperature/temperature.module';
import { CheckWork } from './checkwork/entities/checkwork.entity';
import { CheckWorkModule } from './checkwork/checkwork.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'sqlite',
      database: 'mydb.sqlite',
      entities: [User, CheckWork],
      synchronize: true,
    }),
    TemperatureModule,
    UsersModule,
    CheckWorkModule,
  ],
  controllers: [AppController],
  providers: [AppService],
  exports: [],
})
export class AppModule {
  constructor(private dataSource: DataSource) { }
}
